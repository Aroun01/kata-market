package src.Market;

import src.Exception.InvalidProduitException;
import src.Exception.InvalidQuantiteException;

import java.util.ArrayList;
import java.util.List;

public class SuperMarket {

    private List<Receipt> receiptList = new ArrayList<>();
    private List<Product> productList = new ArrayList<>();
    private List<Discount> promoList = new ArrayList<>();

    public List<Receipt> getMyCart() {
        return receiptList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public int produitsInFcature() {
        return receiptList.size();
    }

     public List<Product> initializeFacture() {
        return productList;
    }

    public void addProduitToReceipe(Product product) throws InvalidProduitException {
        String serialNumber = product.getSerialNumber();
        boolean hasSerialNo = (serialNumber != null && !serialNumber.equals(""));
        if (hasSerialNo
                && product.getProduiPrice().compareTo(Double.valueOf(0)) > 0
                && product.getStoreQuantite() > 0) {
            productList.add(product);
        }
        else {
            throw new InvalidProduitException("Quantite du produit invalide");
        }
    }

    // method to add discount
    public void addPromo(Discount discount) {
        if (discount.getPromoPrice().compareTo(Double.valueOf(0)) > 0 && discount.getPromoQuantite() > 0) {
            promoList.add(discount);
        }
    }

    public boolean addProduitsToFacture(Product product, int qantite) throws InvalidQuantiteException {
        if (qantite <= product.getStoreQuantite()) {
            return receiptList.add(new Receipt(product, qantite));
        } else {
            throw new InvalidQuantiteException("Quantite du produit supérieur à la quantité du stock");
        }
    }


   // method to extract total amount after the discount
   public Double totalValueFacture() {
       Double facturePrice = Double.valueOf(0);
        for (Receipt receipt : receiptList) {
            for (Product product : productList) {
                if (receipt.getProduct().getProduitName().equals(product.getProduitName())) {
                    Double individualProduitPrice = product.getProduiPrice()*(Double.valueOf(receipt.getQuatite()));
                    facturePrice = facturePrice+individualProduitPrice;
                }
            }
        }
       Double promotionValue = calculatePromotion();
        return facturePrice-promotionValue;
    }


    //Method to get promotion for each product based on the quantity
    public Double calculatePromotion() {
        Double promotionValue = Double.valueOf(0);
        for (Receipt receipt : receiptList) {
            for (Discount discount : promoList) {
                Product product = receipt.getProduct();
                if (product.getProduitName().equals(discount.getProduitOnPromo())) {
                    if (receipt.getQuatite() >= discount.getPromoQuantite()) {
                        Double produitPrice = product.getProduiPrice();
                        Double promotionApplyTimes = Double.valueOf(receipt.getQuatite() / discount.getPromoQuantite());
                        Double qunatiteWithoutPromoLeft = Double.valueOf(receipt.getQuatite() % discount.getPromoQuantite());

                        Double priceWithoutDiscount = produitPrice*(Double.valueOf(receipt.getQuatite()));
                        Double priceWithDiscount = discount.getPromoPrice()*promotionApplyTimes
                                +(produitPrice*(qunatiteWithoutPromoLeft));

                        promotionValue = promotionValue+(priceWithoutDiscount-(priceWithDiscount));
                    }
                }
            }
        }
        return promotionValue;
    }

}
