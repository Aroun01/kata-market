package src.Market;

import java.math.BigDecimal;

public class Product {
     private final Boolean hasPromotion;
     private final String serialNumber;
     private final String produitName;
     private final Double produiPrice;
     private final Integer storeQuantite;

     public Product(String serialNumber, String produitName, Double produiPrice, Integer storeQuantite, Boolean hasPromotion) {
          this.hasPromotion = hasPromotion;
          this.serialNumber = serialNumber;
          this.produitName = produitName;
          this.produiPrice = produiPrice;
          this.storeQuantite = storeQuantite;
     }

     public Boolean getHasPromotion() {
          return hasPromotion;
     }

     public String getSerialNumber() {
          return serialNumber;
     }

     public String getProduitName() {
          return produitName;
     }

     public Double getProduiPrice() {
          return produiPrice;
     }

     public Integer getStoreQuantite() {
          return storeQuantite;
     }
}
