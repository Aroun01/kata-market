package src.Market;

public class Receipt {

    private final Product product;
    private final int quatite;

    public Receipt(Product product, int quatite) {
        this.product = product;
        this.quatite = quatite;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuatite() {
        return quatite;
    }
}
