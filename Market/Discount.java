package src.Market;

import java.math.BigDecimal;

public class Discount {

    private final String serialNumber;
    private final String produitOnPromo;
    private final Double promoPrice;
    private final Integer promoQuantite;

    public Discount(String serialNumber, String produitOnPromo, Double promoPrice, Integer promoQuantite) {
        this.serialNumber = serialNumber;
        this.produitOnPromo = produitOnPromo;
        this.promoPrice = promoPrice;
        this.promoQuantite = promoQuantite;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getProduitOnPromo() {
        return produitOnPromo;
    }

    public Double getPromoPrice() {
        return promoPrice;
    }

    public Integer getPromoQuantite() {
        return promoQuantite;
    }
}
