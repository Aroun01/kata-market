package src.Test;

import org.junit.Before;
import org.junit.Test;
import src.Exception.InvalidProduitException;
import src.Exception.InvalidQuantiteException;
import src.Market.Product;
import src.Market.Discount;
import src.Market.SuperMarket;

import java.util.List;

import static org.junit.Assert.*;

public class SuperMarketTest {

    private SuperMarket superMarket;

    private Product lait = new Product("1", "Lait", Double.valueOf(5), 200, true);
    private Product pain = new Product("2", "Pain", Double.valueOf(11.25), 300, true);
    private Product banane = new Product("3", "Banane", Double.valueOf(12), 50, false);
    private Product viande = new Product("4", "Viande", Double.valueOf(526), 70, false);




    @Before
    public void setUp() {
        superMarket = new SuperMarket();
    }

    private void initializeFacture() throws InvalidProduitException {
        superMarket.addProduitToReceipe(lait);
        superMarket.addProduitToReceipe(pain);
        superMarket.addProduitToReceipe(banane);
        superMarket.addProduitToReceipe(viande);
    }

    private void includePromotions() {
        Discount lait = new Discount("1", "Lait", Double.valueOf(15), 4);
        Discount pain = new Discount("2", "Pain", Double.valueOf(30), 3);
        superMarket.addPromo(lait);
        superMarket.addPromo(pain);
    }



    @Test
    public void getPromotionValue() throws InvalidProduitException, InvalidQuantiteException{
        initializeFacture();
        includePromotions();
        superMarket.addProduitsToFacture(pain, 3);
        superMarket.addProduitsToFacture(lait, 4);

        assertEquals(superMarket.calculatePromotion(), (Double.valueOf(3.75+5)));
    }

    @Test
    public void shouldPromotionValue() throws InvalidProduitException, InvalidQuantiteException {
        initializeFacture();
        includePromotions();
        superMarket.addProduitsToFacture(lait, 8);
        superMarket.addProduitsToFacture(pain, 3);

        assertEquals(superMarket.totalValueFacture(), (Double.valueOf(60.00)));
    }

    @Test
    public void checkTheStoreInventory() {
        List<Product> products = superMarket.initializeFacture();
        assertTrue(products.isEmpty());

    }

    @Test
    public void checkProduitsWithInvalidAttributesCannotBeAddedToFacture() throws InvalidProduitException {
        Product axe = new Product("11b", "Axe", Double.valueOf(14), 50, false);
        Product dentifrice = new Product("2a", "Dentifrice", Double.valueOf(5), 100, false);
        Product crayon = new Product("100", "Crayon", Double.valueOf(15), 2, false);

        superMarket.addProduitToReceipe(axe);
        superMarket.addProduitToReceipe(dentifrice);
        superMarket.addProduitToReceipe(crayon);
        assertFalse(superMarket.getProductList().isEmpty());
    }





}
